import React from 'react';
import './App.css';

// Component
import Index from './movie/component/Index.js';

function App() {
  return (
    <>
      <Index />
    </>
  );
}

export default App;
