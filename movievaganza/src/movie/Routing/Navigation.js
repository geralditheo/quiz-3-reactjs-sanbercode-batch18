import React, { useContext } from 'react';
import {Link} from "react-router-dom";

import logo from '../img/logo.png'

// Auth
import { AuthContext } from '../context/Auth/AuthContext';



const Navigation = () => {
    // uth
    const [auth, setAuth] = useContext(AuthContext);

    const handlelogout = () => {
        setAuth({authenficated: false})
    }

    return (
        <>
        <header className='topnav' id='navbar' >
            <img  src={logo} className='logo' />
            <nav className='topnav-right' >
                <ul>
                    <a> <li> <Link to='/'>Home</Link> </li> </a>
                    <a> <li> <Link to='/about' >About</Link> </li> </a>

                    {
                        <>
                        {
                            auth.authenficated  && 
                            <a> <li> <Link to='/crudmovie'>Movie List Editor</Link> </li> </a>
                        }
                        
                        </>
                        
                    }
                    

                    {
                        <>
                            {
                                !auth.authenficated  && 
                                <a> <li>  <Link to='/login' > Login </Link> </li> </a>
                            }
                            {
                                auth.authenficated  && 
                                <a> 
                                    <li onClick={handlelogout} > 
                                         Logout 
                                    </li> 
                                </a>
                            }
                            
                        </>
                        
                    }
                </ul>
            </nav>
        </header>
        </>
    );
}

export default Navigation;
