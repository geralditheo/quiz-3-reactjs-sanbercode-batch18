import React, { useContext } from 'react';
import { Switch, Route  } from 'react-router-dom';

// Provider
import {MovieProvider} from '../context/MovieContext.js';

// Routes
import Home from '../component/Home.js';
import About from '../component/About.js';
import CrudMovie from '../component/CrudMovie.js';
import Login from '../component/Login.js';

// Auth 
import {AuthContext} from '../context/Auth/AuthContext.js';

const Routes = () => {
    const [auth, setAuth] = useContext(AuthContext);

    return (
        <>
        <section>
            <Switch>
                <Route exact path='/' >
                    <MovieProvider>
                        <Home />    
                    </MovieProvider>
                </Route>
                <Route path='/about' >
                    <About />
                </Route>
                <Route path='/crudmovie' >
                    {
                        auth.authenficated  && (
                            <MovieProvider>
                                <CrudMovie />
                            </MovieProvider>
                        )
                    }          
                </Route>
                <Route path='/login' >
                    <Login />
                </Route>
            </Switch>
        </section>
        </>
    );
}

export default Routes;