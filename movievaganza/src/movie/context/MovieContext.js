import React, { createContext, useEffect, useState } from 'react';
import Axios from 'axios';

export const MovieContext = createContext();

export const MovieProvider = props => {
    const [data, setData] = useState(null);

    useEffect(()=>{
        if (data === null){
            Axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then((result) => {
                setData(result.data.map( item => {
                    return {
                        id: item.id,
                        title: item.title,
                        description: item.description,
                        year: item.year,
                        duration: item.duration,
                        genre: item.genre,
                        rating: item.rating,
                        image_url: item.image_url,
                    };
                }))
            })
        }
    },[data])

    return (
        <MovieContext.Provider value={[data, setData]} >
            { props.children }
        </MovieContext.Provider>
    );
}