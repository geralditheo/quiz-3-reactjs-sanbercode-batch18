import React, { createContext, useContext } from 'react';
import {MovieContext} from '../context/MovieContext'

// style
import '../css/style.css'

const Home = () => {
    const [data, setData] = useContext(MovieContext);

    return (
        <>
            <section>
                <h1>Daftar Film Terbaik  </h1>
                {
                    data !== null && 
                        data.map( (item, index) => {
                            return (
                                <>
                                

                                <div key={item.id} >
                                    <h3>Judul {item.title} </h3>

                                    <div>
                                        <img src={item.image_url} width='200px' />
                                        <div className='minidesc' >
                                            <ul>
                                                <strong>
                                                    <li>Rating {item.rating} </li>
                                                    <li>Durasi {item.duration} </li>
                                                    <li>Genre {item.genre} </li>
                                                </strong>
                                            </ul>
                                        </div>
                                        <div>
                                            <p> Deskripsi {item.description} </p>
                                        </div>
                                    </div>
                                </div>
                                </>
                            );
                        })                    
                }
                

                
            </section>
        </>
    );
}

export default Home;