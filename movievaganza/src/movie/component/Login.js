import React, { useContext, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';

// Auth
import {AuthContext} from '../context/Auth/AuthContext.js';

const Login = () => {
    // Declcaration
    const [auth, setAuth] = useContext(AuthContext);
    const [input, setInput] = useState({
        username: '',
        password: '',
        loginError: '',
    });

    const handleChangeInput = (event) => {
        let type = event.target.name;
        let value =event.target.value;
        
        switch(type){
            case 'username' : {
                setInput({...input, username: value});
            }; break;
            case 'password' : {
                setInput({...input, password: value});
            }; break;
        }
    }

    const handleSubmitForm = (event) => {
        event.preventDefault();

        console.log(input.username);
        console.log(input.password);

        console.log(auth.username);
        console.log(auth.password);

        if ( input.username === auth.username && input.password === auth.password ){  
            setAuth({
                authenficated: true,
            })

            return (
                <Link to='/crudmovie' />,
                <Redirect to='/crudmovie' />
            );
                    
        }
    }

    return (
        <>  
            <section>
                <div>
                    <form onSubmit={handleSubmitForm} >
                        <table>
                            <tr>
                                <th>Username</th>
                                <td> <input type='text' name='username' onChange={handleChangeInput} ></input> </td>
                            </tr>

                            <tr>
                                <th>Password</th>
                                <td> <input type='password' name='password' onChange={handleChangeInput} ></input> </td>
                            </tr>
                        </table>

                        <button  > Login </button>
                    </form>
                </div>
            </section>
        </>
    );
}

export default Login;