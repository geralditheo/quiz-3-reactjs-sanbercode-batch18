import Axios from 'axios';
import React, { useContext, useState } from 'react';
import {MovieContext} from '../context/MovieContext.js';

// style
import '../css/style.css'

const CrudMovie = () => {
    // declaration
    const [data, setData] = useContext(MovieContext);
    const [input, setInput] = useState({
        id: null,
        title: '',
        description: '',
        year: null,
        duration: null,
        genre: '',
        rating: null,
        image_url: '',
    });
    
    const [showForm, setShowForm] =useState(false);
    const [callButtonForm, setCallButtonForm] = useState('Tambah');

    const [showUpdate, setShowUpdate] = useState(false);
    const [callButtonEdit, setCallButtonEdit] = useState('Edit');

    // function
    const handleClickDelete = (event) => {
        let id =  parseInt(event.target.value);
        Axios.delete(`http://backendexample.sanbercloud.com/api/movies/${id}`)
        .then((result) => {
            setData(data.filter( item => item.id !== id ));
        })
    }

    const handleClickEdit = (event) => {
        let id = parseInt(event.target.value);

        setInput({...input, id: id});

        setShowUpdate(!showUpdate);
        setShowForm(false);

        if(showUpdate == false){
            setCallButtonEdit("Cancel");
        }else {
            setCallButtonEdit("Edit");
        }
    }

    const handleClickAdd = () => {
        
        setShowForm(!showForm);
        if(showForm == false){
            setCallButtonForm("Sembunyi");
        }else {
            setCallButtonForm("Tambah");
        }

        if(showUpdate == false){
            setCallButtonEdit("Edit");
            setShowUpdate(false);
        }else {
            setCallButtonEdit("Cancel");
            setShowUpdate(true);
        }
        
        
        
    }

    const handleSubmitForm = (event) => {
        event.preventDefault();

        Axios.post(`http://backendexample.sanbercloud.com/api/movies`, 
            {
                title: input.title,
                description: input.description,
                year: input.year,
                duration: input.duration,
                genre: input.genre,
                rating: input.rating,
                image_url: input.url,
            }
        )
        .then((result) => {
            let datum = result.data;
            setData([...data,
                {
                    title: datum.title,
                    description: datum.description,
                    year: datum.year,
                    duration: datum.duration,
                    genre: datum.genre,
                    rating: datum.rating,
                    image_url: datum.url,
                }]
            );

            setInput({
                id: null,
                title: '',
                description: '',
                year: null,
                duration: null,
                genre: '',
                rating: null,
                image_url: '',
            });

            setShowUpdate(false);
        })
    }

    const handleUpdateForm = (event) => {
        event.preventDefault();

        Axios.put(`http://backendexample.sanbercloud.com/api/movies/${input.id}`,{
            title: input.title,
            description: input.description,
            year: input.year,
            duration: input.duration,
            genre: input.genre,
            rating: input.rating,
            image_url: input.url,
        })
        .then((result) => {
            let datum = data.map( item => {
                if (item.id === input.id){
                    item.title = input.title;
                    item.description = input.description;
                    item.year= input.year;
                    item.duration= input.duration;
                    item.genre= input.genre;
                    item.rating= input.rating;
                    item.image_url= input.url;
                }
                return (item);
            })
            setData(datum);

            setInput({
                id: null,
                title: '',
                description: '',
                year: null,
                duration: null,
                genre: '',
                rating: null,
                image_url: '',
            });
        })
    }

    const handleChangeInput = (event) => {
        let type = event.target.name;
        let value = event.target.value;

        switch(type){
            case 'title' : {
                setInput({...input, title: value}); 
            }; break;
            case 'description' : {
                setInput({...input, description: value}); 
            }; break;
            case 'year' : {
                setInput({...input, year: value}); 
            }; break;
            case 'duration' : {
                setInput({...input, duration: value}); 
            }; break;
            case 'genre' : {
                setInput({...input, genre: value}); 
            }; break;
            case 'rating' : {
                setInput({...input, rating: value}); 
            }; break;
            case 'image_url' : {
                setInput({...input, image_url: value}); 
            }; break;
        }


    }

    return(
        <>  
            <section>
                <div>Search</div>

                <div>
                    <h2>Daftar Film</h2>
                    
                    <table>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Year</th>
                                <th>Duration</th>
                                <th>Genre</th>
                                <th>Rating</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody className='headdata' >
                            {
                                data !== null && (
                                    data.map((item, index) => {
                                        return(
                                            <tr key={item.id} >
                                                <td>{index + 1}</td>
                                                <td>{item.title}</td>
                                                <td>{item.description}</td>
                                                <td>{item.year}</td>
                                                <td>{item.duration}</td>
                                                <td>{item.genre}</td>
                                                <td>{item.rating}</td>
                                                <td>
                                                    <button value={item.id} onClick={handleClickEdit} > {callButtonEdit} </button>
                                                    <button value={item.id} onClick={handleClickDelete} >Delete</button>
                                                </td>
                                            </tr>
                                        );
                                    })
                                )
                            }
                            
                        </tbody>
                    </table>  
                </div>

                <div>
                    <button onClick={handleClickAdd} > {callButtonForm} </button>
                </div>
            </section>

            <section>

                {
                    showForm && (
                    <>
                    <h2>Movies Form</h2>

                    <form onSubmit={handleSubmitForm} >
                        <table className='headdata' >
                            <tr>
                                <th>Title :</th>
                                <td> <input type='text' name='title' value={input.title} onChange={handleChangeInput} /> </td>
                            </tr>

                            <tr>
                                <th>Description :</th>
                                <td> <textarea name='description' value={input.description} onChange={handleChangeInput} ></textarea> </td>
                            </tr>

                            <tr>
                                <th>Year :</th>
                                <td> <input type='number' name='year' value={input.year} onChange={handleChangeInput} /> </td>
                            </tr>

                            <tr>
                                <th>Duration :</th>
                                <td> <input type='number' name='duration' value={input.duration} onChange={handleChangeInput} /> </td>
                            </tr>

                            <tr>
                                <th>Genre :</th>
                                <td> <input type='text' name='genre' value={input.genre} onChange={handleChangeInput} /> </td>
                            </tr>

                            <tr>
                                <th>Rating :</th>
                                <td> <input type='text' name='rating' value={input.rating} onChange={handleChangeInput} /> </td>
                            </tr>

                            <tr>
                                <th>Img URL :</th>
                                <td> <input type='text' name='image_url' value={input.image_url} onChange={handleChangeInput} /> </td>
                            </tr>
                        </table>

                        <button>Save</button>
                    </form>
                    </>
                    )
                }

                {
                    showUpdate && (
                    <>
                    <h2>Update Form</h2>

                    <form onSubmit={handleUpdateForm} >
                        <table className='headdata' >
                            <tr>
                                <th>#ID :</th>
                                <td> <input type='text' value={input.id} readOnly /> </td>
                            </tr>

                            <tr>
                                <th>Title :</th>
                                <td> <input type='text' name='title' value={input.title} onChange={handleChangeInput} /> </td>
                            </tr>

                            <tr>
                                <th>Description :</th>
                                <td> <textarea name='description' value={input.description} onChange={handleChangeInput} ></textarea> </td>
                            </tr>

                            <tr>
                                <th>Year :</th>
                                <td> <input type='number' name='year' value={input.year} onChange={handleChangeInput} /> </td>
                            </tr>

                            <tr>
                                <th>Duration :</th>
                                <td> <input type='number' name='duration' value={input.duration} onChange={handleChangeInput} /> </td>
                            </tr>

                            <tr>
                                <th>Genre :</th>
                                <td> <input type='text' name='genre' value={input.genre} onChange={handleChangeInput} /> </td>
                            </tr>

                            <tr>
                                <th>Rating :</th>
                                <td> <input type='text' name='rating' value={input.rating} onChange={handleChangeInput} /> </td>
                            </tr>

                            <tr>
                                <th>Img URL :</th>
                                <td> <input type='text' name='image_url' value={input.image_url} onChange={handleChangeInput} /> </td>
                            </tr>
                        </table>

                        <button>Update</button>
                    </form>
                    </>
                    )
                }
            </section>
        </>
    );
}

export default CrudMovie;