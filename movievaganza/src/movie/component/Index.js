import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";

import '../css/style.css';

// routing
import Navigation from '../Routing/Navigation.js';
import Routes from '../Routing/Routes.js';

// Provider
import {AuthProvider} from '../context/Auth/AuthContext.js';

const Index = () => {
    return (
        <>

            <Router>
                <AuthProvider>
                    <Navigation />
                    <Routes />
                </AuthProvider>
            </Router>
            
        </>
    );
}

export default Index;