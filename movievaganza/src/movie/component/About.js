import React from 'react';

const About = () => {
    return (
        <>
            <section>
                <div>
                    <h2>Data Peserta Sanbercode Bootcamp React JS </h2>

                    <ol>
                        <li>
                            <p> <b>Nama :</b> Geraldi Theo W. K. </p>
                            
                        </li>

                        <li>
                            <p> <b>Email :</b> geralditheo@gmail.com </p>
                            
                        </li>

                        <li>
                            <p> <b> Sistem Operasi  : </b>  Windows 10 </p>
                            
                        </li>

                        <li>
                            <p> <b>Akun Gitlab :</b> @geralditheo </p>
                            
                        </li>

                        <li>
                            <p> <b>Akun Tele :</b> @Geraldi_Theo </p>
                            
                        </li>
                    </ol>
                </div>
            </section>
        </>

    );
}

export default About;